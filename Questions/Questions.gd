extends Node

const path_construct = "res://Questions/QST_Construct.txt"
const path_start = "res://Questions/QST_Start.txt"
const path_logica = "res://Questions/QST_Logica.txt"

const path_Aconstruct = "res://Questions/QST_AConstruct.txt"
const path_Astart = "res://Questions/QST_AStart.txt"
const path_Alogica = "res://Questions/QST_ALogica.txt"


var question_File = File.new()
#\\Arays que guardam todas as perguntas sendo indexadas a partir de 1//#
var questions_Construct = get_QuestionsArray("res://Questions/QST_Construct.txt")
var questions_Start = get_QuestionsArray("res://Questions/QST_Start.txt")
var questions_Logica = get_QuestionsArray("res://Questions/QST_Logica.txt")

#\\Arrays que guardam todas as respostas sendo indexadas a partir de 1//#
var questions_AnsConstruct = get_QuestionsArray("res://Questions/QST_AnsConstruct.txt")
var questions_AnsStart = get_QuestionsArray("res://Questions/QST_AnsStart.txt")
var questions_AnsLogica = get_QuestionsArray("res://Questions/QST_AnsLogica.txt")

#\\Identificador para definir qual a próxima pergunta que deverá ser feita//#
var qCons_ID = 0
var qStar_ID = 0
var qLogi_ID = 0



func _ready():
		
	
	pass

func _process(delta):
	pass


func _input(event):
	
	if event.is_action_pressed("ui_right"):
		$Question.text = change_Question( questions_Construct , qCons_ID , 1)
		$Answer.text = change_Question( path_Aconstruct , qCons_ID , 1)
		qCons_ID  = min(questions_Construct.size() , qCons_ID+1)
	
	
	if event.is_action_pressed("ui_left"):
		if change_Question( questions_Construct , qCons_ID , -1).is_abs_path():
			print(change_Question( questions_Construct , qCons_ID , -1))
			$Sprite.texture = load(change_Question( questions_Construct , qCons_ID , -1).strip_edges())
		else:
			$Question.text = change_Question( questions_Construct , qCons_ID , -1)
			$Answer.text = change_Question( path_Aconstruct , qCons_ID , -1)
		qCons_ID = max(0 , qCons_ID-1)




func get_QuestionsArray( path ):
	var file = File.new()
	var questions = []
	file.open(path , file.READ)
	questions = file.get_as_text().split("</>")
	file.close()
	
	
	return questions
	
	



func change_Question(questions , qID , change):
	qID += change
	
	return questions[qID]


