extends Node

var num_Scenes
var actual_Scene = 0
#var first = true




func _ready():
	var root = get_tree().root.get_node(self.name)
	num_Scenes = $Scenes.get_child_count()
	
	
	for i in range(num_Scenes):
		if i == 0:
			$Scenes.get_child(i).active = true
		else:
			$Scenes.get_child(i).active = false
	
	
	
	if has_node("UI"):
		$UI/Next.connect("pressed" , root , "on_Next_Pressed")
		$UI/Back.connect("pressed" , root , "on_Back_Pressed")


func _process(delta):
	if $UI.transition :
		$UI/Next.hide()
	else:
		$UI/Next.show()
	
	if !System.last_Path and actual_Scene == 0:
		$UI/Back.hide()
	else :
		$UI/Back.show()
	
	if has_node("Progession"):
		if Input.is_action_pressed("ui_accept"):
			$Progession.set_progress(delta)
	if $Scenes.get_child(actual_Scene).has_Boss :
		if Input.is_action_just_pressed("Boss"):
			$Scenes.get_child(actual_Scene).boss_Active = true
		


func to_scene(num):
#	print(actual_Scene)
#	print(num_Scenes)

	if actual_Scene+1 < num_Scenes :
		if actual_Scene == 0 and num < 0:
			num = 0
			
		$Scenes.get_child(actual_Scene).active = false
		actual_Scene += num
		$Scenes.get_child(actual_Scene).active = true
	else:
		if num > 0:
			$UI.transition = true
		else:
			$UI.transition = false
			$Scenes.get_child(actual_Scene).active = false 
			actual_Scene += num
			$Scenes.get_child(actual_Scene).active = true
			
	
		
func path():
	pass

func on_Next_Pressed():
	to_scene(1)
	pass


func on_Back_Pressed():
	if actual_Scene == 0 :
#		load(System.last_Path)
		get_tree().change_scene(System.last_Path)
	else:
		to_scene(-1)
	pass