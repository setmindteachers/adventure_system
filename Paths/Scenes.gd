tool

extends Node


export var active = true
export var boss_Active = false
var has_Boss

var character

func _ready():
	if $ViewportContainer/Viewport.has_node("Boss"):
		has_Boss = true
	else:
		has_Boss = false
	
	if $ViewportContainer/Viewport.get_child_count() :
		character = $ViewportContainer/Viewport.get_child(0)

func _process(delta):
	if !boss_Active:
		if has_node("ViewportContainer/Viewport/Boss"):
			$Sounds/Sound_Effect.stop()
			$ViewportContainer/Viewport/Boss/Boss_SoundEffect.stop()
			$ViewportContainer/Viewport/Boss/Boss_Song.stop()
			$ViewportContainer/Viewport/Boss.hide()
			$ViewportContainer/Viewport/Boss/Camera.current = false
		if !active:
			$"2D".hide()
			$Sounds/Sound_Effect.stop()
			$Sounds/BG_Sound.stop()
			if character:
				character.get_node("Camera").current = false
				character.hide()
		
		else:
			$"2D".show()
			$Sounds/Sound_Effect.play(0.0)
			$Sounds/BG_Sound.play(0.0)
			if character:
				character.get_node("Camera").current = true
				character.show()
	else:
		if has_node("ViewportContainer/Viewport/Boss"):
			
			$Sounds/Sound_Effect.play()
			$ViewportContainer/Viewport/Boss/Boss_SoundEffect.play()
			$ViewportContainer/Viewport/Boss/Boss_Song.play()
			$ViewportContainer/Viewport/Boss.show()
			$ViewportContainer/Viewport/Boss/Camera.current = true
			$"2D".show()
			active = false
