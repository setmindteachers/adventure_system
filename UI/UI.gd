tool

extends Control


export var transition = false


func _ready():
	for i in range($Transition.get_child_count()):
		$Transition.get_child(i).connect("pressed" , self , "change_Scene" ,\
		 [$Transition.get_child(i).path] )
		
	pass

func _process(delta):
	if transition:
		$Transition.show()
	else:
		$Transition.hide()
	
	pass

func change_Scene(path):
#	get_parent().first = false
#	print(get_parent().first)
	
	System.last_Path = get_tree().root.get_child(1).filename
	
	get_tree().change_scene(path)
	
	
	pass

