extends Node2D

export (String , "None" , "Enemie" , "NPC" ) var type 

func _ready():
	if type == "Enemie":
		$LifeBar.show()
	else:
		$LifeBar.hide()
	
	pass


func reset_Progress_Bar():
	if type == "Enemie":
		$LifeBar.region_rect.size.x = 756

func set_progress(delta):
	if type == "Enemie":
		delta *= 300
		$LifeBar.region_rect.size.x = max(0 , $LifeBar.region_rect.size.x-delta)
		