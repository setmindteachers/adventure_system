extends Node2D

var arr = []
var tower = []
var t1 = []
var t2 = []
var t3 = []
var selStatus = "null"
var t0_index = [0,1]
var t1_index = [2,0]
var t2_index = [2,0]
var count = 0

var lastValue = 0
var lastX = 0
var lastY = 0

var thisValue = 0
var thisX = 0
var thisY = 0

func _ready():
	checkStart()
#	for i in range(3):
#		print(i)
	for x in range(3):
		t1.append([])
		for y in range(3):
			t1[x].append([])
			t1[x][y].append([])
			t1[x][y][0] = 0
			t1[x][y].append([])
			t1[x][y][1] = null
	
	#Elements
	t1[0][0][1] = $t/Tower/t0/i0/n
	t1[0][1][1] = $t/Tower/t0/i1/n
	t1[0][2][1] = $t/Tower/t0/i2/n
	t1[1][0][1] = $t/Tower/t1/i0/n
	t1[1][1][1] = $t/Tower/t1/i1/n
	t1[1][2][1] = $t/Tower/t1/i2/n
	t1[2][0][1] = $t/Tower/t2/i0/n
	t1[2][1][1] = $t/Tower/t2/i1/n
	t1[2][2][1] = $t/Tower/t2/i2/n
	
	#Tower
	t1[0][0][0] = 1
	t1[0][1][0] = 2
	t1[0][2][0] = 3
#	t1[1][2][0] = 3
	
	setBoard()
	
	pass

func _physics_process(delta):
		
	pass

func checkStart():
	if lastValue == 0 and lastX == 0 and lastY == 0:
		return true
		

func setBoard():
	for x in range(3):
		for y in range(3):
			var node = t1[x][y][1]
			if t1[x][y][0] == 0:
				node.texture = load("res://Hannoi/Icons/n.png")
			if t1[x][y][0] == 1:
				node.texture = load("res://Hannoi/Icons/n1.png")
			if t1[x][y][0] == 2:
				node.texture = load("res://Hannoi/Icons/n2.png")
			if t1[x][y][0] == 3:
				node.texture = load("res://Hannoi/Icons/n3.png")


func _on_ButtonT0_pressed():
	var validMove = false
	for i in range(3):
		print(i)
		if t1[0][i][0] > 0:
			thisValue = t1[0][i][0]
			thisX = 0
			thisY = i
			validMove = true
			break
		if i == 2 and t1[0][i][0] == 0:
			validMove = false
			selStatus = "null"
			resetIcons()
			thisX = 0
			thisY = i
			thisValue = 0
#	if checkStart():
#		validMove = true
		
	if selStatus == "null" and validMove == true:
		$t/UI/b0/ButtonT0.icon = load("res://Hannoi/Icons/Cancel.png")
		$t/UI/b1/ButtonT1.icon = load("res://Hannoi/Icons/Place.png")
		$t/UI/b2/ButtonT2.icon = load("res://Hannoi/Icons/Place.png")
		lastValue = thisValue
		lastX = thisX
		lastY = thisY
		selStatus = "0"
	
	elif selStatus == "0":
		selStatus = "null"
		resetIcons()
	
	elif selStatus  != "0":
		print("To Nessa")
		if thisValue == 0 and thisY == 2:
			validMove = true
			print("Slot no bottom da matriz valido")
		elif lastValue < t1[thisX][thisY][0] and thisY != 0:
			thisY -= 1
			validMove = true
			print("Há espaço nessa torre")
			print("O valor abaixo nessa torre é maior do que o antigo valor")
		else: validMove = false

		if validMove == true:
			t1[thisX][thisY][0] = t1[lastX][lastY][0]
			t1[lastX][lastY][0] = 0

		resetIcons()
		selStatus = "null"
	printBoard()
	setBoard()
	
func _on_ButtonT1_pressed():
	var validMove = false
	for i in range(3):
		if t1[1][i][0] > 0:
			thisValue = t1[1][i][0]
			thisX = 1
			thisY = i
			validMove = true
			break
		if  i == 2 and t1[1][i][0] == 0:
			validMove = false
			selStatus = "null"
			resetIcons()
			thisX = 1
			thisY = i
			thisValue = 0
	if checkStart():
		validMove = true
			
	if selStatus == "null" and validMove == true:
		if !checkStart():
			$t/UI/b0/ButtonT0.icon = load("res://Hannoi/Icons/Place.png")
			$t/UI/b1/ButtonT1.icon = load("res://Hannoi/Icons/Cancel.png")
			$t/UI/b2/ButtonT2.icon = load("res://Hannoi/Icons/Place.png")
			lastValue = thisValue
			lastX = thisX
			lastY = thisY
			selStatus = "1"
		else: resetIcons()
	elif selStatus == "1":
		selStatus = "null"
		resetIcons()
	
	elif selStatus  != "1":
		print("To aqui")
		if thisValue == 0 and thisY == 2:
			validMove = true
			print("Slot no bottom da matriz valido")
		elif lastValue < t1[thisX][thisY][0] and thisY != 0:
			thisY -= 1
			validMove = true
			print("Há espaço nessa torre")
			print("O valor abaixo nessa torre é maior do que o antigo valor")
		else: validMove = false

		if validMove == true:
			t1[thisX][thisY][0] = t1[lastX][lastY][0]
			t1[lastX][lastY][0] = 0

		resetIcons()
		selStatus = "null"
	printBoard()
	setBoard()

func _on_ButtonT2_pressed():
	var validMove = false
	for i in range(3):
		if t1[2][i][0] > 0:
			thisValue = t1[2][i][0]
			thisX = 2
			thisY = i
			validMove = true
			break
		if  i == 2 and t1[2][i][0] == 0:
			validMove = false
			selStatus = "null"
			resetIcons()
			thisX = 2
			thisY = i
			thisValue = 0
	if checkStart():
		validMove = true
			
	if selStatus == "null" and validMove == true:
		if !checkStart():
			$t/UI/b0/ButtonT0.icon = load("res://Hannoi/Icons/Place.png")
			$t/UI/b1/ButtonT1.icon = load("res://Hannoi/Icons/Place.png")
			$t/UI/b2/ButtonT2.icon = load("res://Hannoi/Icons/Cancel.png")
			lastValue = thisValue
			lastX = thisX
			lastY = thisY
			selStatus = "2"
		else: resetIcons()
	
	elif selStatus == "2":
		selStatus = "null"
		resetIcons()
	
	elif selStatus  != "2":
		print("To aqui")
		if thisValue == 0 and thisY == 2:
			validMove = true
			print("Slot no bottom da matriz valido")
		elif lastValue < t1[thisX][thisY][0] and thisY != 0:
			thisY -= 1
			validMove = true
			print("Há espaço nessa torre")
			print("O valor abaixo nessa torre é maior do que o antigo valor")
		else: validMove = false

		if validMove == true:
			t1[thisX][thisY][0] = t1[lastX][lastY][0]
			t1[lastX][lastY][0] = 0

		resetIcons()
		selStatus = "null"
	printBoard()
	setBoard()

func resetIcons():
	$t/UI/b0/ButtonT0.icon = load("res://Hannoi/Icons/Select.png")
	$t/UI/b1/ButtonT1.icon = load("res://Hannoi/Icons/Select.png")
	$t/UI/b2/ButtonT2.icon = load("res://Hannoi/Icons/Select.png")

func printBoard():
	count+=1
	print(str("##########################",count))
	print(str("[",t1[0][0][0],"]","[",t1[1][0][0],"]","[",t1[2][0][0],"]") )
	print(str("[",t1[0][1][0],"]","[",t1[1][1][0],"]","[",t1[2][1][0],"]") )
	print(str("[",t1[0][2][0],"]","[",t1[1][2][0],"]","[",t1[2][2][0],"]") )
	print("________________________")
	print( str("LastValue:",lastValue,"||LastX:",lastX,"||LastY:",lastY) )
	print( str("ThisValue:",thisValue,"||ThisX:",thisX,"||ThisY:",thisY) )
	





